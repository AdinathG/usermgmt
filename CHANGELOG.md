# Changelog

All Notable changes to `:package_name` will be documented in this file.

Updates should follow the [Keep a CHANGELOG](http://keepachangelog.com/) principles.

## NEXT - YYYY-MM-DD

### Added
- Auth Screens
- Utilities

### Deprecated
- Nothing

### Fixed
- Change in Permisson Controller
- Change in User Controller
- Change in Role Controller
### Removed
- User Creation migration
- Password Reset 

### Security
- Middleware
- Utilities
