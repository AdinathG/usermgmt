# Contributing

Contributions are **welcome** and will be fully **credited**.

We accept contributions via Pull Requests on [Github](https://github.com/:vendor/:package_name).


## Pull Requests

- **[PSR-4 Coding Standard](https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-4-coding-style-guide.md)** - 

- **Add your public key!** - Your pull request will be rejected if don't added key to big bucket.

- **Document any change in behaviour** - Make sure the `README.md` and any other relevant documentation are kept up-to-date.

- **Consider our release cycle** - We try to follow [SemVer v2.0.0](http://semver.org/). Randomly breaking public APIs is not an option.

- **Create feature branches or tags** - Don't ask us to pull from your master branch.It is following v0.1.*

- **One pull request per feature** - If you want to do more than one thing, send multiple pull requests.

- **Send coherent history** - Make sure each individual commit in your pull request is meaningful. If you had to make multiple intermediate commits while developing, please [squash them](http://www.git-scm.com/book/en/v2/Git-Tools-Rewriting-History#Changing-Multiple-Commit-Messages) before submitting.


## Credits

``` bash
Adinath G
Ashidha CB
```


**Happy coding**!
