<?php

namespace Techions\UserMgmt\Models;

use Illuminate\Database\Eloquent\Model;

class permission_role extends Model
{
    public $timestamps=false;

    protected $table = "permissions_roles";
    protected $fillable = ['id','permission_id','user_id','status'];

}
