<?php

namespace Techions\UserMgmt\Models;

use Illuminate\Database\Eloquent\Model;

class role extends Model
{
    protected $fillable = ['id','role_name','status'];
    public $timestamps = false;

    public function users() {
        return $this->belongsToMany('App\Models\API\User','roles_users');
    }

    public function permissions() {
        return $this->belongsToMany('App\Models\permission','permissions_roles');
    }
}
