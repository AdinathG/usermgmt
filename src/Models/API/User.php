<?php

namespace Techions\UserMgmt\Models\API;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Notifications\Notifiable;
use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Log;
use App\Models\permission_screen;

use Illuminate\Database\Eloquent\Model as Eloquent;

class User extends Eloquent implements AuthenticatableContract, CanResetPasswordContract {

    use Authenticatable, CanResetPassword, Notifiable;

    protected $table            =       'users';
    public $timestamps          =        true;
    protected $fillable         =        ['name','email','mobile','type','mobile_bank_id','facebook_url','instagram_url', 'twitter_url'];

    protected $hidden = array('password','created_at', 'updated_at','remember_token','mobile_bank_id', 'image_id');

    public function canAccessScreen($screenName) {
        Log::info("User::canAccessScreen(".$screenName.")");
        $hasAccess = false;
        if($screenName){
            $permissions = session('permissions');


            if(!empty($permissions)){

                foreach($permissions as $perm){
                    $screens = permission_screen::LeftJoin('screens','permissions_screens.screen_id','=','screens.id')
                        ->where('permissions_screens.permission_id', $perm)   ->select('screens.screen_name')-> get();

                    Log::info($screens);

                    if(!empty($screens)){
                        foreach($screens as $screen){
                            if($screen->screen_name == $screenName){
                                $hasAccess = true;
                                break 2;
                            }
                        }
                    }
                }
            }else{
                $screenlist = permission_screen::LeftJoin('screens','permissions_screens.screen_id','=','screens.id')
                    ->where('permissions_screens.permission_id', $permissions)   ->select('screens.screen_name')-> get();
                if(!empty($screenlist)){
                    foreach($screenlist as $slist){
                        if($slist->screens == $screenName){
                            $hasAccess = true;
                            //break 2;
                        }
                    }
                }

            }
        }
        Log::info("hasAccess == ".($hasAccess?"true":"false"));
        return $hasAccess;
    }

    public function roles() {
        return $this->belongsToMany('App\Models\role','roles_users');
    }

    public function feedbacks() {
        return $this->hasMany('App\Models\Feedback');
    }

    public function paymentgatewayusermappings() {
        return $this->hasOne('App\Models\API\PaymentGatewayUserMapping');
    }
}