<?php

namespace Techions\UserMgmt\Models;

use Illuminate\Database\Eloquent\Model;

class permission extends Model
{
    protected $table = "permissions";
    public $timestamps = false;
    protected $fillable = ['id', 'permission_name', 'status'];

    public function roles() {
        return $this->belongsToMany('App\Models\role','permissions_roles');
    }

    public function screens() {
        return $this->belongsToMany('App\Models\Screen','permissions_screens');
    }


}
