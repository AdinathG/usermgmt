<?php

namespace Techions\UserMgmt\Models;

use Illuminate\Database\Eloquent\Model;

class role_user extends Model
{
    public $timestamps = false;
    protected $table = "roles_users";
    protected $fillable = ['id','role_id','user_id','status'];
}
