<?php

namespace Techions\UserMgmt\Models;

use Illuminate\Database\Eloquent\Model;

class Screen extends Model
{
    protected $fillable = ['id','screen_name','status'];
    public $timestamps = false;


    public function permissions() {
        return $this->belongsToMany('App\Models\permission','permissions_screens');
    }
}
