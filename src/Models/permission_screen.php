<?php

namespace Techions\UserMgmt\Models;

use Illuminate\Database\Eloquent\Model;

class permission_screen extends Model
{
    //
    protected $table = "permissions_screens";

    protected $fillable         =       ['id',
        'screen_id',
        'permission_id',
    ];
    public $timestamps = false;
}
