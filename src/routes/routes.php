<?php
/**
 * Created by PhpStorm.
 * User: adinath
 * Date: 22/8/17
 * Time: 5:10 PM
 */

// User management
Route::get('users/list','Techions\UserMgmt\UserManagement\UserController@listUsers');
Route::get('users/add','Techions\UserMgmt\UserManagement\UserController@addUser');
Route::post('users/save','Techions\UserMgmt\UserManagement\UserController@saveUser');
Route::get('users/data','Techions\UserMgmt\UserManagement\UserController@getData');
Route::get('users/edit/{id}','Techions\UserMgmt\UserManagement\UserController@editUser');
Route::post('users/update','Techions\UserMgmt\UserManagement\UserController@updateUser');
Route::get('users/delete/{id}','Techions\UserMgmt\UserManagement\UserController@deleteUser');
Route::get('users/activate/{id}','Techions\UserMgmt\UserManagement\UserController@activateUser');
Route::get('users/deactivate/{id}','Techions\UserMgmt\UserManagement\UserController@deactivateUser');
Route::get('change_password','Techions\UserMgmt\UserManagement\UserController@changePassword');
Route::post('update_password','Techions\UserMgmt\UserManagement\UserController@updatePassword');


//Roles
Route::get('users/addrole','Techions\UserMgmt\UserManagement\RoleController@addRole');
Route::post('users/saverole','Techions\UserMgmt\UserManagement\RoleController@saveRole');
Route::get('users/roles','Techions\UserMgmt\UserManagement\RoleController@listRoles');
Route::get('roles/data','Techions\UserMgmt\UserManagement\RoleController@getData');
Route::get('users/editrole/{id}','Techions\UserMgmt\UserManagement\RoleController@editRole');
Route::post('users/updaterole','Techions\UserMgmt\UserManagement\RoleController@updateRole');
Route::get('users/deleterole/{id}','Techions\UserMgmt\UserManagement\RoleController@deleteRole');

//Permissions
Route::get('users/addpermission','Techions\UserMgmt\UserManagement\PermissionController@addPermissions');
Route::post('users/savepermission','Techions\UserMgmt\UserManagement\PermissionController@savePermission');
Route::get('permissions/data','Techions\UserMgmt\UserManagement\PermissionController@getData');
Route::get('users/permissions','Techions\UserMgmt\UserManagement\PermissionController@listPermissions');
Route::get('users/editpermission/{id}','Techions\UserMgmt\UserManagement\PermissionController@editPermissions');
Route::post('users/updatepermission','Techions\UserMgmt\UserManagement\PermissionController@updatePermission');
Route::get('users/deletepermission/{id}','Techions\UserMgmt\UserManagement\PermissionController@deletePermission');
/*=======================================================================================================*/
