<?php

namespace Techions\UserMgmt;

use App\Http\Middleware\AuthorizeScreens;
use Illuminate\Support\ServiceProvider;

class UserMgmtServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        require __DIR__.'/routes/routes.php';
        $this->loadViewsFrom(base_path('resources/views'),'UserMgmt');
        $this->publishes([
            __DIR__.'/views'=> base_path('resources/views')
        ]);
        $this->publishes([
            __DIR__.'/migrations'=> database_path('migrations')
        ],'migrations');
        $this->publishes([
            __DIR__.'/PermissionUtil.php' => base_path('app/Utils/PermissionUtil.php'),
        ]);
        $this->app['router'];

    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->make('Techions\UserMgmt\UserManagement\PermissionController');
        $this->app->make('Techions\UserMgmt\UserManagement\UserController');
        $this->app->make('Techions\UserMgmt\UserManagement\RoleController');
        $this->app['router']->middleware('AuthorizeScreens', 'App\Http\Middleware\AuthorizeScreens');
        $this->app->bind('Techions-UserMgmt',function (){
           return new SkeletonClass();
        });
    }
}