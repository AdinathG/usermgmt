@extends('layouts.app')

@section('content')

	<div class="content-wrapper">

		<!-- Page header -->
		<div class="page-header page-header-default">
			<div class="page-header-content">
				<div class="page-title">
					<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Edit</span> - Role</h4>
				</div>


			</div>

			<div class="breadcrumb-line">
				<ul class="breadcrumb">
					<li><a href="{{url('/home')}}"><i class="icon-home2 position-left"></i> Home</a></li>
					<li><a href="{{url('users/roles')}}">User Management</a></li>
					<li class="active">Role</li>
				</ul>


			</div>
		</div>
		<!-- /page header -->


		<!-- Content area -->
		<div class="content">

			<!-- Info alert -->

			<!-- /info alert -->


			<!-- Sidebars overview -->
			<div class="panel panel-flat panel-body">
				<div class="panel-heading">
					<h5 class="panel-title">Role</h5>
				</div>

				<div class="panel-body">

				</div>
				<div id="msg" class=""></div>

	{!!  Form::open(array('url'=>'users/updaterole','id'=>'editrole','method'=>'POST', 'class' => 'form-horizontal'))  !!}
	{!! Form::hidden('id',$value = $role->id,['id'=>'id']) !!}
	<!-- Text input-->
	<div class="form-group">
		<label class="col-md-4 control-label " for="rolename" >Rolename</label>
		<div class="col-md-4">

			{!! Form::text('role_name',$value =$role->role_name,['class'=>'form-control','placeholder'=>'rolename']) !!}

		</div>
	</div>

	<!-- Multiple Checkboxes -->
	<div class="form-group">
		<label class="col-md-4 control-label" for="roles">Permissions</label>
		<div class="col-md-4 scrollable">
			@foreach ($permissions as $permission)
			@if(in_array($permission->id,$permissionsChecked))
			<div class="checkbox">
				<label for="permissions-0">
					<input type="checkbox" checked="checked" name="permissions[]"  value={{$permission->
					id}}>
					{{$permission->permission_name}} </label>
			</div>
			@else
			<div class="checkbox">
				<label for="permissions-1">
					<input type="checkbox" name="permissions[]" value={{$permission->
					id}}>
					{{$permission->permission_name}} </label>
			</div>
			@endif
			@endforeach
		</div>
	</div>

	<!-- Button (Double) -->
	    <div class="form-group">
		<label class="col-md-4 control-label" for="save"></label>
		<div class="col-md-2">
			{!!Form::submit('Submit',['class'=>'btn bg-teal-400'])!!}
		</div>
	</div>
	{!! Form::close() !!}
			</div></div></div>
@endsection

@push('scripts')

{!! HTML::script('assets/js/pages/form_validation.js') !!}
<script>
	$(document).ready(function() {

		$("#editrole").validate({
			rules : {
				rolename : {
					required : true,
				},
				permissions : {
					required : true,
				},
			},
			messages : {
				rolename : "Please enter role name!",
				permissions : "Please select permissions",
			}
		});
	}); 
</script>
@endpush

