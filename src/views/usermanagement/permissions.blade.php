@extends('layouts.app')

@section('content')
    <div class="content-wrapper">

        <!-- Page header -->

        <?php

        $perm_screen_data = session('permission_screens');
        Log::info("Permissions Screen  :-  ", [$perm_screen_data]);

        ?>

        <div class="page-header page-header-default">
            <div class="page-header-content">
                <div class="page-title">
                    <div class="col-md-10" align="left">
                        <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Details</span> -
                            Permissions</h4>
                    </div>
                    <div>
                        @if(in_array('add_permission',$perm_screen_data))
                            <div align="right">
                                <a href="{{asset('users/addpermission')}}" class="btn button_color">
                                    Add Permission <i
                                            class="icon-play3"></i></a>
                            </div>
                        @endif
                    </div>

                </div>


            </div>


            <div class="breadcrumb-line">
                <ul class="breadcrumb">
                    <li><a href="{{url('/home')}}"><i class="icon-home2 position-left"></i> Home</a></li>
                    <li><a href="{{url('users/permissions')}}">User Management</a></li>
                    <li class="active">Permissions</li>
                </ul>


            </div>
        </div>
        <!-- /page header -->


        <!-- Content area -->
        <div class="content">

            <!-- Info alert -->

            <!-- /info alert -->


            <!-- Sidebars overview -->
            <div class="panel panel-flat">


                <div id="msg" class=""></div>
                <table class="table datatable-basic" id="permissions-table">
                    <thead>
                    <tr>
                        <th>Sl No</th>
                        <th>PermissionName</th>
                        <th>Actions</th>

                    </tr>
                    </thead>
                </table>

                <!-- <div class="footer text-muted">
                    &copy; 2015. <a href="#">ePRO</a> by <a href="http://themeforest.net/user/Kopyov" target="_blank">Eugene
                        Kopyov</a>
                </div> -->
            </div>

        </div>
    </div>

@endsection

@push('scripts')
{!! HTML::script('assets/js/plugins/tables/datatables/datatables.min.js',array(),true) !!}

{!! HTML::script('assets/js/pages/datatables_sorting.js',array(),true) !!}

{!! HTML::script('assets/js/pages/components_modals.js',array(),true) !!}
<script>
    var table;
    $(document).ready(function () {
        table = $('#permissions-table').DataTable({
            "bDestroy": true,
            "columnDefs": [{
                "defaultContent": "-",
                "targets": "_all"
            }],
            ajax: "{{asset('permissions/data')}}",
            columns: [{
                data: "id"
            }, {
                data: null,
                render: function (data, type, row) {
                    // Combine the first and last names into a single table field
                    return data.permission_name;
                }
            },
                    @if(in_array('edit_permission',$perm_screen_data))
                {
                    data: null,
                    render: function (data, type, row) {
                        if (data.status == 1) {
                            var editurl = "{{asset('users/editpermission')}}/" + data.id
                            var editfield = '<a id="edit" href="' + editurl + '" class="editor_edit btn btn-sm btn-teal"><i class="glyphicon glyphicon-edit"></i></a>';

                            var deletefield = '<button class="editor_remove btn btn-sm btn-danger" data-id=' + data.id + '><i class="icon-close2 icon-white"></i></button>';

                        } else {
                            var editfield = '';
                            var deletefield = '<button class="editor_remove btn btn-sm btn-success" data-id=' + data.id + '>Enable</button>';
                        }
                        @if(in_array('delete_permission',$perm_screen_data))
                            return editfield + ' ' + deletefield;

                        @else
                            return editfield;
                        @endif

                    }
                }
                @endif
            ],
            "order": [[1, 'asc']]
        });
        table.on('order.dt', function () {
            table.column(0, {
                order: 'applied'
            }).nodes().each(function (cell, i) {
                cell.innerHTML = i + 1;
            });
        }).draw();

        $('#permissions-table').on('click', 'button.editor_remove', function (e) {
            var id = $(this).attr('data-id');
            console.log(id);
            var result = confirm("Are you sure you want to delete?");
            if (result) {
                $.ajax({

                    type: 'GET',
                    url: "{{asset('users/deletepermission')}}/" + id,
                    dataType: 'json',
                    success: function (data) {
                        console.log(data);
                        table.ajax.reload();
                        var message = "Permission deleted successfully";
                        $('#msg').show();
                        $('#msg').removeClass().addClass('alert alert-success').html(message);
                        $('html,body').animate({
                                scrollTop: $(".page-header").offset().top
                            },
                            500);
                        $('#msg').delay(3200).fadeOut(300);
                    }
                })
            }
        });
    });

</script>
@endpush
