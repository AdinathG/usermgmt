@extends('layouts.app')
@section('content')
	<div class="content-wrapper">

		<!-- Page header -->
		<div class="page-header page-header-default">
			<div class="page-header-content">
				<div class="page-title">
					<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Edit</span> - Permission</h4>
				</div>


			</div>

			<div class="breadcrumb-line">
				<ul class="breadcrumb">
					<li><a href="{{url('/home')}}"><i class="icon-home2 position-left"></i> Home</a></li>
					<li><a href="{{url('users/permissions')}}">User Management</a></li>
					<li class="active">Permission</li>
				</ul>

			</div>
		</div>
		<!-- /page header -->


		<!-- Content area -->
		<div class="content">

			<!-- Info alert -->

			<!-- /info alert -->


			<!-- Sidebars overview -->
			<div class="panel panel-flat panel-body">
				<div class="panel-heading">
					<h5 class="panel-title">Permission</h5>
				</div>

				<div class="panel-body">

				</div>
				<div id="msg" class=""></div>

	{!!  Form::open(array('url'=>'users/updatepermission','id'=>'editpermission','method'=>'POST', 'class' => 'form-horizontal'))  !!}

	{!! Form::hidden('id',$value = $permission->id,['id'=>'id']) !!}

	<div class="row">
		<div class="col-md-12">
			<div class="form-group">
				<label class="col-md-4 control-label " for="permissionname" >Permission Name</label>
				<div class="col-md-4">

					{!! Form::text('permissionname',$value = $permission->permission_name,['class'=>'form-control','id'=>'permissionname']) !!}
				</div>
			</div>

		</div>

		<div class="col-md-12">
			<h6 class="col-md-12 text-semibold">Screens</h6>
			</br></br>
			<?php
			$alreadyscreens = implode(",", $perm_screen);
			?>
			<input type="hidden" name="screens_sel" id="screens_sel" value="{{$alreadyscreens}}">
			@foreach ($screens as $screen)
			<?php $screenid = $screen -> id; ?>
				@if(count($childscreens[$screenid])>0)
			<div class="col-md-4">
				<input type="checkbox" class="selectall" name="screen[]" id="selectall_{{$screenid}}" value="{{$screenid}}"
				@if (in_array($screenid, $parent_screen_data)) checked="checked" @endif>

				<strong>{{$screen->screen_name}}</strong></input>

				@foreach ($childscreens[$screenid] as $child)
				<div class="col-md-12">
					<input name="childscreen[]" class="singlechild childscreen_{{$screenid}}" type="checkbox" id="childscreen_{{$screenid}}" value="{{$child->id}}"
					@if (in_array($child->id, $perm_screen)) checked="checked" @endif >

					{{$child->screen_name}}</input>
				</div>

				@endforeach

			</div>
			@endif
			@endforeach
		</div>
		<div class="col-md-12">
			<label class="col-md-4 control-label" for="save"></label>
			<div class="col-md-2">
				{!!Form::submit('Submit',['class'=>'btn bg-teal-400'])!!}
			</div>
		</div>

	</div>

	{!! Form::close() !!}
			</div></div></div>
@endsection

@push('scripts')
<script>
	$(document).ready(function() {

		function disableBack() { window.history.forward() }

		window.onload = disableBack();
		window.onpageshow = function(evt) { if (evt.persisted) disableBack() }

		$("#addpermission").validate({
			rules : {
				permissionname : {
					required : true,
				},
			},
			messages : {
				permissionname : "Please enter permission name!",
			}
		});

		var checkedArray = new Array();

		var screens_sel = $('#screens_sel').val();

		if (screens_sel != "") {
			var checkedArray = screens_sel.split(",");
		}

		console.log("first");
		console.log(checkedArray);

		$('.selectall').on('click', function() {
			var id = $(this).attr('id');

			if (id) {
				var newid = id.substring('selectall_'.length - 1);
				var screenid = "#selectall" + newid;
				var childclass = '.childscreen' + newid;
				var result = $(screenid).val();
				console.log(result);

				if (this.checked) {
					$(childclass).each(function() {//loop through each checkbox
						this.checked = true;
						var jsonArg = $(this).val();
						checkedArray.push(jsonArg);
						return jsonArg;
						//select all checkboxes with class "checkbox1"
					});
				} else {
					$(childclass).each(function() {//loop through each checkbox
						this.checked = false;
						var jsonArg = $(this).val();						
						checkedArray.splice(checkedArray.indexOf(jsonArg), 1 );
						return jsonArg;
						//deselect all checkboxes with class "checkbox1"
					});
				}

			}

			
			$('#screens_sel').val(checkedArray);

		});

		$('.singlechild').on('click', function() {
			var id = $(this).attr('id');

			if (id) {
				var newid = id.substring('childscreen_'.length - 1);
				var screenid = "#childscreen" + newid;
				if (this.checked) {
					var jsonArg = $(this).val();
					console.log(jsonArg);
					checkedArray.push(jsonArg);
					console.log(checkedArray);
				
				} else {
					var jsonArg = $(this).val();						
					checkedArray.splice(checkedArray.indexOf(jsonArg), 1 );
					
				}
				console.log(checkedArray);
				$('#screens_sel').val(checkedArray);
			}

		});

	}); 
</script>
@endpush

