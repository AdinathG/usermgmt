@extends('layouts.app')
@section('content')
    <div class="content-wrapper">

        <!-- Page header -->
        <div class="page-header page-header-default">
            <div class="page-header-content">
                <div class="page-title">
                    <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Add</span> - Roles</h4>
                </div>
            </div>


            <div class="breadcrumb-line">
                <ul class="breadcrumb">
                    <li><a href="{{url('/home')}}"><i class="icon-home2 position-left"></i> Home</a></li>
                    <li><a href="{{url('users/roles')}}">User Management</a></li>
                    <li class="active">Roles</li>
                </ul>
            </div>

        </div>
        <!-- /page header -->


        <!-- Content area -->
        <div class="content">

            <!-- Info alert -->

            <!-- /info alert -->


            <!-- Sidebars overview -->
            <div class="panel panel-flat panel-body">
               


                
                <div id="msg" class=""></div>

        {!!  Form::open(array('url'=>'users/saverole','id'=>'addrole','method'=>'POST', 'class' => 'form-horizontal'))  !!}


<div class="panel panel-default">
                <div class="panel-body">
			<!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label " for="rolename" >Role Name</label>
                    <div class="col-md-4">
                   
                   	{!! Form::text('role_name',$value = Input::old('role_name'),['class'=>'form-control','id'=>'role_name']) !!}
                    </div>
                </div>

                <!-- Multiple Checkboxes -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="Permissions">Permissions</label>
                    <div class="col-md-4  scrollable">
                      @foreach ($permissions as $permission)
                            <div class="checkbox">
                                <label for="Permissions-0">
                                    <input type="checkbox" name="permissions[]"  value={{ $permission->id}}>
                                    {{ $permission->permission_name}}

                                </label>
                            </div>

                       @endforeach
                    </div>
                </div>



                <!-- Button (Double) -->
               <div class="form-group">
		<label class="col-md-4 control-label" for="save"></label>
		<div class="col-md-2">
			{!!Form::submit('Submit',['class'=>'btn bg-teal-400'])!!}
		</div>
	</div></div></div>
        {!! Form::close() !!}
        </div>
        </div></div>
@endsection

@push('scripts')

{!! HTML::script('assets/js/pages/form_validation.js') !!}
     <script>
	$(document).ready(function() {
		$("#addrole").validate({
			rules : {
				role_name : {
					required : true,
				},
				permissions : {
					required : true,
				},
			},
			messages : {
				role_name : "Please enter role name!",
				permissions : "Please select permissions",
			}
		});
	}); 
</script>
@endpush

