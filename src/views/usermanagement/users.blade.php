@extends('layouts.app')

@section('content')
    <div class="content-wrapper">

    <!--
    <?php
        \App\Utils\PermissionUtil::getScreenData();
        $perm_screen_data = session('permission_screens');
        Log::info("Permissions Screen  :-  ", [$perm_screen_data]);
    ?>
            -->
        <!-- Page header -->
        <div class="page-header page-header-default">
            <div class="page-header-content">
                <div class="page-title">
                    <div class="col-md-10" align="left">
                        <h4><i class="icon-arrow-left52 position-left"></i> <span
                                    class="text-semibold">Details</span> -
                            Users</h4>
                    </div>
                    <div>

                        @if(in_array('add_user',$perm_screen_data))
                            <div align="right"><a href="{{asset('users/add')}}" class="btn button_color"> Add User <i
                                            class="icon-play3"></i></a></div>
                        @endif
                    </div>
                </div>


            </div>

            <div class="breadcrumb-line">
                <ul class="breadcrumb">
                    <li><a href="{{url('/home')}}"><i class="icon-home2 position-left"></i> Home</a></li>
                    <li><a href="{{url('users/list')}}">User Management</a></li>
                    <li class="active">Users</li>
                </ul>


            </div>
        </div>
        <!-- /page header -->


        <!-- Content area -->
        <div class="content">
            <div class="panel panel-flat">

                <div class="row">
                    <div class="col-md-12">
                        @foreach($errors->all() as $error)
                            <div class="errorHandler alert alert-danger">
                                <i class="icon-remove-sign"></i> {!!$error!!}
                            </div>
                        @endforeach
                    </div>
                </div>
                <div id="msg" class=""></div>
                <table class="table datatable-sorting" id="users-table">
                    <thead>
                    <tr>
                        <th>Sl No</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Type</th>
                        <!-- <th>Sites</th> -->
                        <th>Actions</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

@stop

@push('scripts')

{!! HTML::script('assets/js/plugins/tables/datatables/datatables.min.js',array(),true) !!}

{!! HTML::script('assets/js/pages/datatables_sorting.js',array(),true) !!}

{!! HTML::script('assets/js/pages/components_modals.js',array(),true) !!}

<script>
    $('.errorHandler').delay(3200).fadeOut(300);
    var table;
    $(document).ready(function () {
        table = $('#users-table').DataTable({
            "bDestroy": true,
            "columnDefs": [{
                "defaultContent": "-",
                "targets": "_all"
            }],
            ajax: "{{asset('users/data')}}",
            columns: [
                {data: "id"},
                {data: "name"},
                {data: "email"},
                {
                    data: null, render: function (data, type, row) {
                    console.log(data);
                    var userType = "";
                    switch (data.type) {
                        case 0:
                            userType = "Admin";
                            break;

                        case "1":
                            userType = "Dog owner";
                            break;

                        case "2":
                            userType = "Dog sitter";
                            break;

                        case "3":
                            userType = "Admin user";
                            break;

                    }
                    return userType;
                }
                },
                {
                    data: null, render: function (data, type, row) {
                    //console.log(data);
                    if (data.status == 1 || data.status == 2) {
                                @if(in_array('edit_user',$perm_screen_data))
                        var editurl = "{{asset('users/edit')}}/" + data.id;
                        var editfield = '<a id="edit" href="' + editurl + '" class="editor_edit btn btn-sm btn-teal"><i class=" glyphicon glyphicon-edit"></i></a>';
                                @else
                        var editfield = "";
                                @endif

                                @if(in_array('delete_user',$perm_screen_data))
                        var deletefield = '<button class="editor_remove btn btn-sm btn-danger" data-id=' + data.id + '><i class="icon-trash icon-white"></i></button>';
                                @else
                        var deletefield = "";
                        @endif

                        if (data.status == 1) {
                                    @if(in_array('deactivate_user',$perm_screen_data))
                            var activatefield = '<button class="editor_deactivate btn btn-sm btn-warning" data-id=' + data.id + '><i class="icon-close2 icon-white"></i></button>';
                                    @else
                            var activatefield = "";
                            @endif
                        } else {
                                    @if(in_array('activate_user',$perm_screen_data))
                            var activatefield = '<button class="editor_activate btn btn-sm btn-success" data-id=' + data.id + '><i class="glyphicon glyphicon-ok icon-white"></i></button>';
                                    @else
                            var activatefield = "";
                            @endif

                        }

                    }
                    return editfield + ' ' + deletefield + ' ' + activatefield;
                }
                }

            ],
            "order": [[1, 'asc']]
        });
        table.on('order.dt', function () {
            table.column(0, {
                order: 'applied'
            }).nodes().each(function (cell, i) {
                cell.innerHTML = i + 1;
            });
        }).draw();

        $('#users-table').on('click', 'button.editor_activate', function (e) {
            var id = $(this).attr('data-id');
            $.ajax({
                type: 'GET',
                url: "{{asset('users/activate')}}/" + id,
                dataType: 'json',
                success: function (data) {
                    console.log(data);
                    table.ajax.reload();
                    $('html,body').animate({
                        scrollTop: $(".page-header").offset().top
                    }, 500);

                    $('#msg').delay(3200).fadeOut(300);
                }

            })

        });

        $('#users-table').on('click', 'button.editor_deactivate', function (e) {
            var id = $(this).attr('data-id');
            $.ajax({
                type: 'GET',
                url: "{{asset('users/deactivate')}}/" + id,
                dataType: 'json',
                success: function (data) {
                    console.log(data);
                    table.ajax.reload();
                    $('html,body').animate({
                        scrollTop: $(".page-header").offset().top
                    }, 500);

                    $('#msg').delay(3200).fadeOut(300);
                }

            })

        });

        $('#users-table').on('click', 'button.editor_remove', function (e) {
            var id = $(this).attr('data-id');
            console.log(id);
            var result = confirm("Are you sure you want to delete?");
            if (result) {
                $.ajax({


                    type: 'GET',
                    url: "{{asset('users/delete')}}/" + id,
                    dataType: 'json',
                    success: function (data) {
                        console.log(data);
                        table.ajax.reload();
                        if (data == 1) {
                            var message = "User deleted successfully";
                            $('#msg').show();
                            $('#msg').removeClass().addClass('alert alert-success').html(message);
                            table.ajax.reload();
                        } else if (data == 0) {
                            var message = "You can't delete yourself";
                            $('#msg').show();
                            $('#msg').removeClass().addClass('alert alert-danger').html(message);
                        }

                        $('html,body').animate({
                            scrollTop: $(".page-header").offset().top
                        }, 500);

                        $('#msg').delay(3200).fadeOut(300);
                    }

                })
            }
        });
    });

</script>
@endpush