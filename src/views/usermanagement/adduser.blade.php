@extends('layouts.app')
@section('content')
	<div class="content-wrapper">

		<!-- Page header -->
		<div class="page-header page-header-default">
			<div class="page-header-content">
				<div class="page-title">
					<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Add</span> - Users</h4>
				</div>


			</div>


			<div class="breadcrumb-line">
				<ul class="breadcrumb">
					<li><a href="{{asset('home')}}"><i class="icon-home2 position-left"></i> Home</a></li>
					<li><a href="{{asset('users/list')}}">User Management</a></li>
					<li class="active">User</li>
				</ul>


			</div>
		</div>
		<!-- /page header -->


		<!-- Content area -->
		<div class="content">

			<!-- Info alert -->

			<!-- /info alert -->


			<!-- Sidebars overview -->
			<div class="panel panel-flat panel-body">
				<div class="panel-heading">
					<h5 class="panel-title">User</h5>
				</div>

				<div class="panel-body">

				</div>
				<div id="msg" class=""></div>

				<div class="row">
					<div class="col-md-12">
						{!! Form::open(array('url'=>'users/save','id'=>'adduser','method'=>'POST', 'class' => 'form-register')) !!}



						<div class="panel panel-default">
							<div class="panel-body">

								<fieldset>
									<div class="row">
										<div class="col-md-12">
											@foreach($errors->all() as $error)
												<div class="errorHandler alert alert-danger">
													<i class="icon-remove-sign"></i> {!!$error!!}
												</div>
											@endforeach
										</div>

										<div class="col-md-6">

											<div class="form-group has-feedback has-feedback-left">
												{!! Form::text('firstname',Input::old('firstname'),['id'=>'firstname','class'=>'form-control','placeholder'=>'Name','style'=>'width:100%;']) !!}
												<div class="form-control-feedback">
													<i class="  icon-user"></i>
												</div><div id="error-firstname"></div>
											</div>


											<div class="form-group has-feedback has-feedback-left">

												{!! Form::number('phone',Input::old('phone'),['id'=>'phone','class'=>'form-control','placeholder'=>'Mobile']) !!}
												<div class="form-control-feedback">
													<i class=" icon-mobile"></i>
												</div>
											</div>


												<div class="form-group ">

													{!! Form::select('roles[]',$roles,null,['class' => 'form-control search-select','multiple'=>'multiple','id' => 'roles']) !!}
													<div class="form-control-feedback">
														<i class=" icon-users"></i>
													</div>
												</div>

											<div class="form-group has-feedback has-feedback-left">

												{!! Form::password('password',['id' => 'password','class' => 'form-control','placeholder'=>'Password']) !!}
												<div class="form-control-feedback">
													<i class=" icon-key"></i>
												</div>
												<div id="error-password"></div>
											</div>


											{{--<div class="form-group">--}}
												{{--{!! Form::select('userTypes',  $userTypes , null,['class' => 'form-control search-select','id' => 'userType']) !!}--}}
												{{--<div class="form-control-feedback">--}}
													{{--<i class=" icon-users"></i>--}}
													{{--<div id="error-usertype"></div>--}}
												{{--</div>--}}
											{{--</div>--}}
										</div>

										<div class="col-md-6">

											<div class="form-group">
												<div class="form-group has-feedback has-feedback-left">
													{!! Form::text('email',Input::old('email'),['id'=>'email','class'=>'form-control','placeholder'=>'Email']) !!}
													<div class="form-control-feedback">
														<i class=" icon-mention"></i>
													</div>
													<div id="error-email"></div>
												</div>


											</div>


											<div class="form-group has-feedback has-feedback-left">
												<select name="status" id="status" class="form-control" >
													<option value="1" class="label-success">Active</option>
													<option value="2" class="label-danger">Inactive</option>
												</select>
												<div class="form-control-feedback">
													<i class=" icon-bookmark2"></i>
												</div>
											</div>
											<div class="form-group has-feedback has-feedback-left">
												{!! Form::password('confirmPassword',['id' => 'confirmPassword','class' => 'form-control','placeholder'=>'Confirm Password']) !!}
												<div class="form-control-feedback">
													<i class=" icon-key"></i>
												</div>
												<div id="error-confirmPassword"></div>
											</div>

										</div>


									</div>

									<div class="row">
										<div class="col-md-12" align="center" style="padding-top: 15px;">
											{!!Form::submit('Submit',['class'=>'btn bg-teal-400 btn-block','style'=>'width:70px;'])!!}
										</div>
									</div>

								</fieldset>

							</div>
						</div>

						{!! Form::close() !!}
					</div>
				</div>
			</div></div></div>
@endsection

@push('scripts')

{!! HTML::script('assets/js/pages/form_multiselect.js') !!}

<script>
    $(document).ready(function() {
        $('.search-select').select2();

        $('#userTypes').select2({
            placeholder: "Select user type"
        });

        $.validator.addMethod("valueNotEquals", function (value, element, arg) {
            return value != arg;
        });

        $("#adduser").validate({
            rules : {
                firstname : {
                    required : true,
                    minlength : 2
                },
                lastname :{
                    required : true
                },
                email : {
                    required : true,
                    email : true
                },
                password :{
                    required:true,
                    minlength:5
                },
                confirmPassword: {
                    required: true,
                    minlength: 5,
                    equalTo: "#password"
                },
                userTypes : {
                    valueNotEquals: -1
                }

            },
            messages : {
                firstname : "Please enter first name!",
                email : "Please enter a valid email address",
                userTypes : "Please choose a user type"

            },
            errorPlacement: function (error, element) {
                var name = $(element).attr("name");
                if(name)
                {
                    error.appendTo($("#error-"+name));
                }
                else
                {
                    error.insertAfter(element);
                }

            },
        });


    });
</script>

@endpush
