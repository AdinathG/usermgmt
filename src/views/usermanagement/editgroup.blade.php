@extends('index')

@section('content')

    <div class="content-wrapper">

        <!-- Page header -->
        <div class="page-header page-header-default">
            <div class="page-header-content">
                <div class="page-title">
                    <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Edit</span> - Group</h4>
                </div>


            </div>


            <div class="breadcrumb-line">
                <ul class="breadcrumb">
                    <li><a href="{{url('/home')}}"><i class="icon-home2 position-left"></i> Home</a></li>
                    <li><a href="{{url('users/sites')}}">User Management</a></li>
                    <li class="active">Site</li>
                </ul>


            </div>
        </div>
        <!-- /page header -->


        <!-- Content area -->
        <div class="content">

            <!-- Info alert -->

            <!-- /info alert -->


            <!-- Sidebars overview -->
            <div class="panel panel-flat panel-body">
               
                <div id="msg" class=""></div>

        {!!  Form::open(array('url'=>'users/updatesite','id'=>'editgroup','method'=>'POST', 'class' => 'form-horizontal'))  !!}
        {!! Form::hidden('id',$value = $site->id,['id'=>'id']) !!}
  
            <div class="panel panel-default">
                  <div class="panel-body">
                     <fieldset>
                        <div class="row">
                           <div class="col-md-12">
                              @foreach($errors->all() as $error)
                              <div class="errorHandler alert alert-danger">
                                 <i class="icon-remove-sign"></i> {!!$error!!}
                              </div>
                              @endforeach
                           </div>
                               <div class="col-md-6 ">
                                  <!-- <div class="form-group marginCss">
                                     <label class="control-label" >Name</label>
                                     {!! Form::text('name',$site->name,['id'=>'name','class'=>'form-control','style'=>'width:100%;']) !!}
                                  </div> -->
                                  <div class="form-group has-feedback has-feedback-left marginCss">
                                      {!! Form::text('name',$site->name,['id'=>'name','class'=>'form-control','placeholder'=>'Name','style'=>'width:100%;']) !!}
                                      <div class="form-control-feedback">
                                      <i class="fa fa-hospital-o"></i>
                                       </div>
                                </div>
                                 <!--  <div class="form-group marginCss">
                                     <label class="control-label" >Address</label>
                                     {!! Form::text('address',$site->address,['id'=>'address','class'=>'form-control','style'=>'width:100%;']) !!}
                                  </div> -->
                                  <div class="form-group has-feedback has-feedback-left marginCss">

                                {!! Form::text('address',$site->address,['id'=>'address','class'=>'form-control','placeholder'=>'Address','style'=>'width:100%;']) !!}
                                    <div class="form-control-feedback">
                                        <i class="  icon-address-book"></i>
                                    </div>
                                </div>
                               </div>
                               <div class="col-md-6">
                                <!--   <div class="form-group marginCss">
                                    <label class="control-label" >Country</label>

                                {!! Form::select('country',($country),$site->country,['class' => 'form-control','id' => 'nationality']) !!}
                                </div> -->
                                <div class="form-group has-feedback has-feedback-left marginCss">
                                      {!! Form::select('country',($country),$site->country,['class' => 'form-control','id' => 'country']) !!}
                                      <div class="form-control-feedback">
                                      <i class="  icon-city"></i>
                                       </div>
                                </div>
                               <!--  <div class="form-group marginCss">
                                     <label class="control-label" >Pin</label>
                                     {!! Form::text('pin',$site->pin,['id'=>'pin','class'=>'form-control','style'=>'width:100%;']) !!}
                                  </div> -->

                                   <div class="form-group has-feedback has-feedback-left marginCss">
                                    {!! Form::text('pin',$site->pin,['id'=>'pin','class'=>'form-control','placeholder'=>'Pincode','style'=>'width:100%;']) !!}
                                    <div class="form-control-feedback">
                                        <i class=" icon-mailbox"></i>
                                    </div>
                                </div>
                               </div>
                        </div>
                        <div class="row">
                           <div class="col-md-3" align="center" style="padding-top: 15px;">
                              {!!Form::submit('Submit',['class'=>'btn bg-teal-400 btn-block'])!!}
                           </div>
                        </div>
                     </fieldset>
                  </div>
               </div>
            
        {!! Form::close() !!}
        </div>
            </div></div></div>
@endsection

@push('scripts')

{!! HTML::script('assets/js/pages/form_validation.js') !!}
    <script>
	$(document).ready(function() {

		$("#editgroup").validate({
			rules : {
				groupname : {
					required : true,
				},
				permissions : {
					required : true,
				},
			},
			messages : {
				groupname : "Please enter group name!",
				permissions : "Please select permissions",
			}
		});
	}); 
</script>
@endpush

