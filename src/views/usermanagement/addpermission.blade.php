@extends('layouts.app')
@section('content')
    <div class="content-wrapper">

        <!-- Page header -->
        <div class="page-header page-header-default">
            <div class="page-header-content">
                <div class="page-title">
                    <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Add</span> -
                        Permission</h4>
                </div>


            </div>

            <div class="breadcrumb-line">
                <ul class="breadcrumb">
                    <li><a href="{{url('/home')}}"><i class="icon-home2 position-left"></i> Home</a></li>
                    <li><a href="{{url('users/permissions')}}">User Management</a></li>
                    <li class="active">Permission</li>
                </ul>

            </div>
        </div>
        <!-- /page header -->


        <!-- Content area -->
        <div class="content">

            <!-- Info alert -->

            <!-- /info alert -->


            <!-- Sidebars overview -->
            <div class="panel panel-flat panel-body">

                <div id="msg" class=""></div>

                {!!  Form::open(array('url'=>'users/savepermission','id'=>'addpermission','method'=>'POST', 'class' => 'form-horizontal'))  !!}


                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-md-4 control-label " for="permissionname">Permission Name</label>
                                    <div class="col-md-4">

                                        {!! Form::text('permission_name',$value = Input::old('permission_name'),['class'=>'form-control','id'=>'permission_name']) !!}
                                    </div>
                                </div>

                            </div>
                            @if(!empty($screens))
                                <div class="col-md-12">
                                    <label class="col-md-4 control-label ">Screens</label></br></br>
                                    <input type="hidden" name="screens_sel" id="screens_sel" value="">

                                    @foreach ($screens as $screen)
                                        <?php $screenid = $screen->id; ?>
                                        @if(count($childScreens[$screenid]) > 0)
                                            <div class="col-md-4">
                                                <input type="checkbox" class="selectall" name="screen[]"
                                                       id="selectall_{{$screenid}}" value="{{$screenid}}">

                                                <strong>{{$screen->screen_name}}</strong></input>

                                                @foreach ($childScreens[$screenid] as $child)
                                                    <div class="col-md-12">
                                                        <input name="childscreen[]"
                                                               class="singlechild childscreen_{{$screenid}}"
                                                               type="checkbox" id="childscreen_{{$screenid}}"
                                                               value="{{$child->id}}">

                                                        {{$child->screen_name}}</input>
                                                    </div>
                                                @endforeach

                                            </div>
                                        @endif
                                    @endforeach
                                </div>
                                <div class="col-md-12">
                                    <label class="col-md-4 control-label" for="save"></label>
                                    <div class="col-md-2">
                                        {!!Form::submit('Submit',['class'=>'btn bg-teal-400'])!!}
                                    </div>
                                </div>

                            @else
                                <div class="col-md-12">
                                    <label class="col-md-4 control-label" for="save"></label>

                                    NO SCREENS ARE ADDED FOR THIS GROUP

                                </div>
                            @endif

                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

@push('scripts')

<script>
    $(document).ready(function () {

        $("#addpermission").validate({
            rules: {
                permission_name: {
                    required: true,
                },
            },
            messages: {
                permission_name: "Please enter permission name!",
            }
        });

        var checkedArray = new Array();

        $('.selectall').on('click', function () {
            var id = $(this).attr('id');

            if (id) {
                var newid = id.substring('selectall_'.length - 1);
                var screenid = "#selectall" + newid;
                var childclass = '.childscreen' + newid;
                var result = $(screenid).val();
                console.log(result);

                if (this.checked) {
                    $(childclass).each(function () {//loop through each checkbox
                        this.checked = true;
                        var jsonArg = $(this).val();
                        checkedArray.push(jsonArg);
                        return jsonArg;
                        //select all checkboxes with class "checkbox1"
                    });
                } else {
                    $(childclass).each(function () {//loop through each checkbox
                        this.checked = false;
                        var jsonArg = $(this).val();
                        checkedArray.splice(checkedArray.indexOf(jsonArg), 1);
                        return jsonArg;
                        //deselect all checkboxes with class "checkbox1"
                    });
                }

            }

            console.log(checkedArray);
            $('#screens_sel').val(checkedArray);

        });

        $('.singlechild').on('click', function () {
            var id = $(this).attr('id');

            if (id) {
                var newid = id.substring('childscreen_'.length - 1);
                var screenid = "#childscreen" + newid;
                if (this.checked) {
                    var jsonArg = $(this).val();
                    console.log(jsonArg);
                    checkedArray.push(jsonArg);
                    console.log(checkedArray);

                } else {
                    var jsonArg = $(this).val();
                    checkedArray.splice(checkedArray.indexOf(jsonArg), 1);

                }
                console.log(checkedArray);
                $('#screens_sel').val(checkedArray);
            }

        });


    });
</script>
@endpush

