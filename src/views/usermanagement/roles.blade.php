@extends('layouts.app')
@section('content')
    {!! HTML::style('assets/css/style.css',array(),true) !!}
    <div class="content-wrapper">

    <?php
    \App\Utils\PermissionUtil::getScreenData();
    $perm_screen_data = session('permission_screens');
    ?>

    <!-- Page header -->
        <div class="page-header page-header-default">
            <div class="page-header-content">
                <div class="page-title">
                    <div class="col-md-10" align="left">
                        <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Details</span> -
                            Roles</h4>
                    </div>
                    <div>
                        @if(in_array('add_role',$perm_screen_data))
                            <div align="right">
                                <a href="{{asset('users/addrole')}}" class="btn button_color">
                                    Add Role <i class="icon-play3"></i></a>
                            </div>
                        @endif
                    </div>
                </div>

            </div>

            <div class="breadcrumb-line">
                <ul class="breadcrumb">
                    <li><a href="{{url('/home')}}"><i class="icon-home2 position-left"></i> Home</a></li>
                    <li><a href="{{url('users/roles')}}">User Management</a></li>
                    <li class="active">Roles</li>
                </ul>


            </div>
        </div>
        <!-- /page header -->


        <!-- Content area -->
        <div class="content">
            <!-- Sidebars overview -->
            <div class="panel panel-flat">

                <div id="msg" class=""></div>
                <table class="table datatable-sorting" id="roles-table">
                    <thead>
                    <tr>
                        <th>Sl No</th>
                        <th>RoleName</th>
                        <th>Actions</th>

                    </tr>
                    </thead>
                </table>
                <!-- <div class="footer text-muted">
                    &copy; 2015. <a href="#">ePRO</a> by <a href="http://themeforest.net/user/Kopyov" target="_blank">Eugene
                        Kopyov</a>
                </div> -->

            </div>
        </div>
    </div>


@stop

@push('scripts')
{!! HTML::script('assets/js/plugins/tables/datatables/datatables.min.js',array(),true) !!}

{!! HTML::script('assets/js/pages/datatables_sorting.js',array(),true) !!}

{!! HTML::script('assets/js/pages/components_modals.js',array(),true) !!}


<script>

    var table;
    $(document).ready(function () {
        table = $('#roles-table').DataTable({
            "bDestroy": true,
            "columnDefs": [{
                "defaultContent": "-",
                "targets": "_all"
            }],
            ajax: "{{asset('roles/data')}}",
            columns: [{
                data: "id"
            }, {
                data: null,
                render: function (data, type, row) {
                    // Combine the first and last names into a single table field
                    return data.role_name;
                }
            }, {
                data: null,
                render: function (data, type, row) {
                    if (data.status == 1) {
                        var editurl = "{{asset('users/editrole')}}/" + data.id
                        var editfield = '<a id="edit" href="' + editurl + '" class="editor_edit btn btn-sm btn-teal"><i class="glyphicon glyphicon-edit"></i></a>';

                        var deletefield = '<button class="editor_remove btn btn-sm btn-danger" data-id=' + data.id + '><i class="icon-close2 icon-white"></i></button>';

                    } else {
                        var editfield = '';
                        var deletefield = '<button class="editor_remove btn btn-sm btn-success" data-id=' + data.id + '>Enable</button>';
                    }

                    return editfield + ' ' + deletefield;
                }
            }],
            "order": [[1, 'asc']]
        });
        table.on('order.dt', function () {
            table.column(0, {
                order: 'applied'
            }).nodes().each(function (cell, i) {
                cell.innerHTML = i + 1;
            });
        }).draw();

        $('#roles-table').on('click', 'button.editor_remove', function (e) {
            var id = $(this).attr('data-id');
            console.log(id);
            var result = confirm("Are you sure you want to delete?");
            if (result) {
                $.ajax({

                    type: 'GET',
                    url: "{{asset('users/deleterole')}}/" + id,
                    dataType: 'json',
                    success: function (data) {
                        console.log(data);
                        table.ajax.reload();
                        var message = "Role deleted successfully";
                        $('#msg').show();
                        $('#msg').removeClass().addClass('alert alert-success').html(message);
                        $('html,body').animate({
                                scrollTop: $(".page-header").offset().top
                            },
                            500);
                        $('#msg').delay(3200).fadeOut(300);
                    }
                })
            }
        });
    });

</script>


@endpush
