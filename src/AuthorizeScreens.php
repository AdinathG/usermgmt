<?php

namespace App\Http\Middleware;

use Closure;
use App;
use Response;
use Route;
use Log;
use Illuminate\Contracts\Auth\Guard;

use App\Models\Screen;
use App\Models\ScreenMapping;
use App\Utils\PermissionUtil;

class AuthorizeScreens
{


    protected $auth;

    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    public function handle($request, Closure $next) {
        $isPermitted = true;
        if ($this->auth->guest()) {
            if ($request->ajax()) {
                return response('Unauthorized.', 401);
            } else {
                return redirect()->guest('login');
            }
        }

        PermissionUtil::getScreenData();
        $permissionScreens = session('permission_screens');

        $currentAction = Route::currentRouteAction();
        list($controller, $method) = explode('@', $currentAction);
        $controller = explode("\\", $controller);

        $controllerName = $controller[count($controller) - 1];

        $permitted_controllers = array('Controller', 'HomeController');


        if (in_array($controllerName, $permitted_controllers)) {

            //Log::info("Permitted Controller");

        } else {
            $screens = ScreenMapping::where('controller', $controllerName)
                ->where('method', $method)
                ->get();


            if(count($screens)) {
                foreach ($screens as $screen) {
                    if (in_array($screen->screen, $permissionScreens)) {
                        $isPermitted = true;
                    } else {
                        $isPermitted = false;
                    }
                }
            }else{
                $isPermitted = false;
            }


            if (!$isPermitted) {
                return Response::view('errors/403');
            }
        }
        return $next($request);
    }
}
