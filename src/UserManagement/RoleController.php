<?php

namespace Techions\UserMgmt\UserManagement;

use Techions\UserMgmt\Models\role_user;
use Input;
use Validator;
use Log;
use Session;

use Techions\UserMgmt\Models\role;
use Techions\UserMgmt\Models\permission;
use Techions\UserMgmt\Models\permission_screen;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Yajra\Datatables\Datatables;


class RoleController extends Controller
{

    private $user;

    public function addRole() {
        $permissions = Permission::select('id', 'permission_name')->where('status', 1)->get();
        Log::info($permissions);
        return View('usermanagement/addrole', compact('permissions'));
    }

    protected function saveRole(Request $request) {
        $role = new Role();
        $role->fill($request->all());
        $role->status = 1;
        $role->save();
        $roleid = $role->id;

        $selectedPermissionIds = $request->permissions;

        //$this->user->roles()->attach($roleid);
        $role->permissions()->attach($selectedPermissionIds);

        return redirect('users/roles');
    }

    public function listRoles() {
        return View('usermanagement/roles');
    }

    public function getData() {
        $roles = role::select(array('id', 'role_name', 'status'))
                        ->where('status', 1);
        return Datatables::of($roles)->make(true);
    }


    public function editRole($id) {
        $role = role::find($id);

        $permissions = Permission::select('id', 'permission_name')
                                ->where('status', 1)
                                ->get();

        $checkedPermissions = $role->permissions()
                                ->where('role_id', $id)
                                ->where('status', 1)
                                ->get();

        $permissionsChecked = array();

        foreach ($checkedPermissions as $permission) {
            array_push($permissionsChecked, $permission->id);
        }
        return View('usermanagement/editrole', compact('role', 'permissions', 'permissionsChecked'));
    }

    public function updateRole(Request $request) {

        $role = Role::find($request->get('id'));
        $role->fill(Input::all())->save();

        $selectedPermissionIds = $request->permissions;
        $role->permissions()->detach();
        $role->permissions()->attach($selectedPermissionIds);
        return redirect('users/roles');

    }

    public function deleteRole($id) {
        $role = role::find($id);
        $role->status = 0;
        $role->users()->detach();
        $role->permissions()->detach();
        $role->save();
        return redirect('users/roles');
    }


    public function getPermissions() {
        return Permission::select('id', 'permissionname')->where('status', 1)->get();
    }


    public function addVendorRoles($vendorid) {
        $provider = session('provider');

        if (!empty($provider)) {

            Log::info("provider permissions");

            $permissions = Permission::Join('provider_permission_mapping', 'provider_permission_mapping.permission_id', '=', 'permission.id')
                ->select(array('permission.id', 'permission.permissionname', 'permission.status'))->where('permission.status', 1)->get();
        } else {
            Log::info("full permissions");

            $permissions = Permission::select('id', 'permissionname')->where('status', 1)->get();
        }


        return View('usermanagement/addrole', compact('permissions', 'vendorid'));
    }


}

