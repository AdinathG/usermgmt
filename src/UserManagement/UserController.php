<?php

namespace Techions\UserMgmt\UserManagement;

use Techions\UserMgmt\Models\API\User;
use Illuminate\Support\Facades\Redirect;
use Validator;
use Log;
use Input;
use Session;
use Hash;
use Carbon;
use DB;
use App\Http\Controllers\Controller;
// use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Techions\UserMgmt\Models\role;
use ResetsPasswords;
use Yajra\Datatables\Datatables;

//use Request;

class UserController extends Controller
{

    private $user;

    public function listUsers()
    {
        $user = User::find(Auth::id());
        return View('usermanagement/users')->with('user', $user);
    }

    public function getData()
    {
        $users = User::where('type','<>',0)
                    ->where('status','<>',0)
                    ->get();
        return Datatables::of($users)->make(true);
    }

    public function addUser()
    {
        $userTypes = [0 => 'Admin'];

        $rolesList = $this->getRoles();
        $roles = array();

        if ($rolesList) {
            foreach ($rolesList as $role) {
                $roles[$role->id] = $role->role_name;
            }
        }
        return View('usermanagement/adduser', compact('roles', 'userTypes'));
    }

    public function saveUser(Request $request)
    {

        $user = User::where('email', $request->email)
            ->first();

        if ($user == null) {
            $name = $request->firstname;
            $phone = isset($request->phone) ? $request->phone : '';

            $user = new User();
            $user->email = $request->email;
            $user->name = $name;
            $user->mobile = $phone;
            $user->mobile_bank_id = "000";
            $user->type = 3;
            $user->password = bcrypt($request->password);
            $user->status= $request->status;
            $user->save();

            $userid = $user->id;

            $roleIds = $request->roles;

            if ($roleIds) {
                foreach ($roleIds as $roleId) {
                    $user->roles()->attach($roleId);
                }
            }
        } else {
            return redirect('users/list')->withErrors([
                'email' => 'User already exists',
            ]);
        }

        return redirect('users/list');

    }

    public function editUser($id)
    {
        $user = User::find($id);
        $rolesList = role::select('id', 'role_name')
            ->where('status', 1)
            ->get();

        $userTypes = [0 => 'Admin'];
        $selectedUserType = $user->type;

        $roles = array();
        foreach ($rolesList as $role) {
            $roles[$role->id] = $role->role_name;
        }

        $checkedRolesList = $user->roles()
            ->select('role_id')
            ->where('user_id', '=', $id)
            ->get();

        $checkedRoles = array();

        $index = 0;
        foreach ($checkedRolesList as $checkedRole) {
            $checkedRoles[$index] = $checkedRole->role_id;
            $index++;
        }

        return View('usermanagement/edituser', compact('user', 'roles', 'checkedRoles', 'userTypes', 'selectedUserType'));
    }

    public function updateUser(Request $request)
    {
        $userid = $request->get('id');

        $user = User::find($userid);

        Log::info($request);

        if ($user) {
            $name = $request->firstname . " " . $request->lastname;
            $user->name = $name;
            $user->email = $request->email;
            $user->status= $request->status;
            //$user->type = $request->userTypes;
            if (isset($request->password)) {
                $user->password = bcrypt($request->password);
            }

            $user->mobile = $request->phone;
            $user->save();

            $selectedRoleIds = $request->get('roles');
            $user->roles()->detach();
            $user->roles()->attach($selectedRoleIds);
        }
        return redirect('users/list');

    }

    public function deleteUser($id)
    {
        if ($id != Auth::user()->id) {
            $user = User::find($id);
            $user->status = 0;
            $status = $user->save() ? 1 : 0;
        } else {
            $status = 0;
        }
        return $status;
    }

    public function getRoles()
    {
        $roles = role::select('id', 'role_name')
            ->where('status', 1)
            ->get();
        return $roles;
    }

    public function changePassword() {
        $user = User::find(Auth::id());
        return View('auth.passwords.changePassword', compact('user'));
    }

    public function updatePassword(Request $request) {
        if(Auth::check()) {
            $user = Auth::user();

            $newPassword        = bcrypt($request->password);
            $oldPassword        = $request->oldpassword;

            if (password_verify($oldPassword, $user->password)) {
                $user->password = $newPassword;
                $user->save();
                return Redirect::back()->with('message', 'Password Changed Successfully');
            }
            return Redirect::back()->withErrors(['Authentication Failed']);
        }

        return Redirect::back()->withErrors(['User not found']);
    }

    public function activateUser($id)
    {
        $user = User::find($id);
        $user->status = 1;
        $status = $user->save() ? 1 : 0;
        return $status;
    }

    public function deActivateUser($id)
    {
        $user = User::find($id);
        $user->status = 2;
        $status = $user->save() ? 1 : 0;
        return $status;
    }

}

