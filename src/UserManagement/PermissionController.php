<?php
/**
 * Created by PhpStorm.
 * User: viswajith
 * Date: 31/8/15
 * Time: 12:32 PM
 */

namespace Techions\UserMgmt\UserManagement;

use Techions\UserMgmt\Models\permission_screen;
use Log;
use App\Http\Controllers\Controller;
use DB;
use Session;

use Techions\UserMgmt\Models\API\User;
use Techions\UserMgmt\Models\permission;
use Techions\UserMgmt\Models\Screen;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Validator;
use Illuminate\Support\Facades\Input;
use Yajra\Datatables\Datatables;

class PermissionController extends Controller
{

    private $user;

    public function addPermissions() {
        $childScreens = array();
        $parentScreens = Screen::select('id', 'screen_name', 'parent_screen_id')
            ->where('parent_screen_id', 0)
            ->where('status', 1)
            ->get();

        foreach ($parentScreens as $screen) {
            $screenid = $screen->id;
            $screens = screen::select(array('id', 'screen_name', 'parent_screen_id'))
                ->where('parent_screen_id', $screenid)
                ->get();
            $childScreens[$screenid] = $screens;
        }
        Log::info('child screens : ', [$childScreens]);
        return View('usermanagement/addpermission')->with('screens', $parentScreens)->with('childScreens', $childScreens);
    }

    public function savePermission(Request $request) {
        Log::info('selectedScreens : ' . $request->screens_sel);

        $permission = new Permission();
        $permission->fill($request->all());
        $permission->status = 1;
        $permission->save();

        $permission_id = $permission->id;

        $screens = $request->screens_sel;
        Log::info('selected screens : ' . $screens);

        $screenarray = explode(",", $screens);
        if (!empty($screens)) {
            Log::info($screenarray);
            Log::info(count($screenarray));
            foreach ($screenarray as $screenId) {
                $permission->screens()->attach($screenId);
            }
        }

        return redirect('users/permissions');

    }

    public function listPermissions() {
        $user = User::find(Auth::id());
        return View('usermanagement/permissions')->with('user', $user);
    }

    public function getData() {
        $permissions = permission::select(array('id', 'permission_name', 'status'))
            ->where('status', 1);
        return Datatables::of($permissions)->make(true);
    }

    public function editPermissions($id) {
        $childScreens = array();
        $permission = permission::find($id);

        $parentScreens = screen::select(array('id', 'screen_name', 'parent_screen_id'))
            ->where('parent_screen_id', 0)
            ->where('status', 1)
            ->get();

        foreach ($parentScreens as $screen) {
            $screenid = $screen->id;
            $childscreens = screen::select(array('id', 'screen_name', 'parent_screen_id'))
                ->where('parent_screen_id', $screenid)
                ->get();
            $childScreens[$screenid] = $childscreens;
        }

        $checkedChildScreens = $permission->screens()
            ->where('permission_id', $id)
            ->get();
        $checkedScreenIds = array();

        foreach ($checkedChildScreens as $checkedScreen) {
            array_push($checkedScreenIds, $checkedScreen->id);
        }

        $checkedParentScreens = screen::select('parent_screen_id')
                                ->whereIn('id', $checkedScreenIds)
                                ->groupby('parent_screen_id')
                                ->get();

        $checkedParentScreenIds = array();
        foreach ($checkedParentScreens as $checkedParentScreen) {
          array_push($checkedParentScreenIds, $checkedParentScreen->id);
        }

        return View('usermanagement/editpermission', compact('permission'))
            ->with('screens', $parentScreens)
            ->with('perm_screen', $checkedScreenIds)
            ->with('childscreens', $childScreens)
            ->with('parent_screen_data', $checkedParentScreenIds);

    }

    public function updatePermission(Request $request) {
        $permission = permission::find($request->get('id'));
        $permission->fill(Input::all())->save();
        $selectedScreens = $request->screens_sel;
        if (!empty($selectedScreens)) {
            $selectedScreenIds = explode(",", $selectedScreens);
            $permission->screens()->detach();
            $permission->screens()->attach($selectedScreenIds);
        }
        return redirect('users/permissions');
    }

    public function deletePermission($id) {
        $permission = permission::find($id);
        $permission->status = 0;
        $permission->save();
        $permission->screens()->detach();
        $permission->roles()->detach();
        return redirect('users/permissions');
    }
}
