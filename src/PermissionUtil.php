<?php
/**
 * Created by PhpStorm.
 * User: viswajith
 * Date: 20/5/17
 * Time: 12:01 PM
 */

namespace App\Utils;

use Illuminate\Support\Facades\Auth;

use App\User;
use Log;


class PermissionUtil
{

    // Get user screen data from mapping tables
    public static function getScreenData() {
        $user = self::getUser();
        $permissionScreens = array();
        if ($user) {
            $roles = $user->roles;
            foreach ($roles as $role) {
                $permissions = $role->permissions;
                foreach ($permissions as $permission) {
                    $screens = $permission->screens;
                    foreach ($screens as $screen) {
                        $screenName = $screen->screen_name;
                        if (!in_array($screenName, $permissionScreens)) {
                            array_push($permissionScreens, $screenName);
                        }
                    }
                }
            }
        }
      //Log::info('screen data : ', $permissionScreens);
      session(['permission_screens' => $permissionScreens]);
    }

    // Get the logged in user
    private static function getUser() {
        $user = null;
        if (Auth::check()) {
            $userId = Auth::user()->id;
            $user = User::find($userId);

        }
        return $user;
    }


}